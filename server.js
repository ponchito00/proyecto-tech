//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin",'*');
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca4m44626/collections/movimientos?apiKey=caFRWFHXR_btpKEZKsywyaXOWfEBY4Q5";
var clientesMlab = requestjson.createClient(urlmovimientosMlab);

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ("/",function(req,res) {
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get ("/clientes/:idcliente",function(req,res) {
  res.send('Aqui tiene al cliente nùmero :' + req.params.idcliente);
});


app.post("/",function(req,res){
  res.send('Hemos recibido su peticion cambiada');
});

app.delete("/",function(req,res){
  res.send('Hemos recibido su peticion de delete');
});

app.put("/",function(req,res){
  res.send('Hemos recibido su peticion de actualizar');
});

app.get ("/movimientos", function(req, res) {
clientesMlab.get('',function(err, resM, body){
 if(err){
   console.log(body);
 } else {
   res.send(body);
 }
});
});

app.post("/movimientos", function(req, res){
    clientesMlab.post('', req.body, function(err, resM, body){
      res.send(body);
    });
  });
